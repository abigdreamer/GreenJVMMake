package org.test.jvm;

import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.util.HashSet;
import java.util.Set;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

public class JarUtil {
	
	private static final String JAVA_VERSION = System.getProperty("java.version");
	private static final String JAVA_SPECIFICATION_VERSION = System.getProperty("java.specification.version");

	public static final Set<String> getClasses(String applicationJarPath) {
		HashSet<String> result = new HashSet<String>();
		try {
			FileInputStream inputStream = new FileInputStream(applicationJarPath);
			BufferedInputStream bufferedInputStream = new BufferedInputStream(inputStream);
			ZipInputStream zipInputStream = new ZipInputStream(bufferedInputStream);
			ZipEntry zipEntry = null;
			while ((zipEntry = zipInputStream.getNextEntry()) != null) {
				String entryName = zipEntry.getName();
				if (entryName.endsWith("class")) {
					result.add(entryName.replace(".class", "").replace('/', '.'));
				}
			}
			zipInputStream.close();
		} catch (Exception localException) {
			throw new RuntimeException(localException);
		}
		return result;
	}

	public static final String buildMetaInfo() {
		StringBuffer result = new StringBuffer();
		result.append("Manifest-Version: 1.0\n");
		result.append("Implementation-Vendor: Sun Microsystems, Inc.\n");
		result.append("Implementation-Title: Java Runtime Environment\n");
		result.append(("Implementation-Version: " + JAVA_VERSION + "\n").intern());
		result.append("Specification-Vendor: Sun Microsystems, Inc.\n");
		result.append(("Created-By: " + JAVA_VERSION + " (Sun Microsystems Inc.)\n").intern());
		result.append("Specification-Title: Java Platform API Specification\n");
		result.append(("Specification-Version: " + JAVA_SPECIFICATION_VERSION).intern());
		return result.toString();
	}
}
